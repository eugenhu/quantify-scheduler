# Profiling

We provide tools for measuring and analyzing the run time of `quantify-scheduler` and its components, consisting of a profiling notebook and three schedule notebooks, capturing different use cases. 

Please consult the [profiling notebook](profiling.ipynb) for further information on using it. The notebooks can be downloaded here:
- {download}`profiling.ipynb`
- {download}`simple_binned_acquisition.ipynb`
- {download}`resonator_spectroscopy.ipynb`
- {download}`random_gates.ipynb`

```{toctree}
:maxdepth: 1
:hidden:

profiling.ipynb
```
